package edu.sjsu.android.accgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

public class SimulationView extends View implements SensorEventListener {
    private Bitmap mField;
    private Bitmap mBasket;
    private Bitmap mBitmap;

    private static final int BALL_SIZE = 64;
    private static final int BASKET_SIZE = 80;

    private float mXOrigin;
    private float mYOrigin;

    private float mHorizontalBound;
    private float mVerticalBound;

    private float sensor_x;
    private float sensor_y;
    private float sensor_z;
    private long Sensor_timeStamp;

    private Display display;
    private SensorManager sensorManager;
    private  Particle ball = new Particle();

    @Override
    public void onSizeChanged(int weight, int height, int oldWeight, int oldHeight){
        mXOrigin = weight * 0.5f;
        mYOrigin = height * 0.5f;

        mHorizontalBound = (weight - BALL_SIZE) * 0.5f;
        mVerticalBound = (height - BALL_SIZE) * 0.5f;
    }

    public SimulationView(Context context){
        super(context);
        // Initialize images from drawable
        Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.basketball);
        mBitmap = Bitmap.createScaledBitmap(ball, BALL_SIZE, BALL_SIZE, true);
        Bitmap basket = BitmapFactory.decodeResource(getResources(), R.drawable.basket);
        mBasket = Bitmap.createScaledBitmap(basket, BASKET_SIZE, BASKET_SIZE, true);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.RGB_565;
        mField = BitmapFactory.decodeResource(getResources(), R.drawable.basket_field, opts);

        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();

        sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        startSimulation();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent){
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            accelerometerData(sensorEvent);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy){

    }

    private void accelerometerData(SensorEvent sensorEvent){
        sensor_x = sensorEvent.values[0];
        sensor_y = sensorEvent.values[1];
        sensor_z = sensorEvent.values[2];

        Sensor_timeStamp = System.nanoTime();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            if(display.getRotation() == Surface.ROTATION_0){
                sensor_x = sensorEvent.values[0];
                sensor_y = sensorEvent.values[1];
            }else if(display.getRotation() == Surface.ROTATION_90){
                sensor_x = -sensorEvent.values[0];
                sensor_y = sensorEvent.values[1];
            }
        }
    }


    public void startSimulation() {
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopSimulation() {
        sensorManager.unregisterListener(this);
    }
    @Override
    protected  void onDraw(Canvas canvas){
        super.onDraw(canvas);

        canvas.drawBitmap(mField,0,0,null);
        canvas.drawBitmap(mBasket, mXOrigin - BASKET_SIZE / 2, mYOrigin - BASKET_SIZE / 2, null);

        ball.updatePosition(sensor_x, sensor_y, sensor_z, Sensor_timeStamp);
        ball.resolveCollisionWithBounds(mHorizontalBound, mVerticalBound);

        canvas.drawBitmap(mBitmap,
                (mXOrigin - BASKET_SIZE / 2) + ball.mPosX,
                (mYOrigin - BASKET_SIZE / 2) - ball.mPosY,null);
        invalidate();
    }
}
